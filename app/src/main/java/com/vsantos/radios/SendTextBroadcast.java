package com.vsantos.radios;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vsantos.radios.realm.UserRealm;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by vitor on 2/27/16.
 */
public class SendTextBroadcast extends AppCompatActivity {
    Button btnSend;
    EditText editText;
    Toolbar toolbar;
    Intent mIntent;
    RequestQueue rq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast_send_text);

        toolbar = (Toolbar)findViewById(R.id.toolBar);
        toolbar.setTitle("");
        toolbar.setNavigationIcon(R.mipmap.ic_back_arrow);

        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        rq = Volley.newRequestQueue(this);

        mIntent = getIntent();

        btnSend = (Button)findViewById(R.id.btnSend);
        editText = (EditText)findViewById(R.id.editText);


        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().length() > 5){
                    sendTicker();
                }else{
                    editText.setError(getString(R.string.error_edit_text_very_short));
                }
            }
        });



    }

    private void sendTicker() {

        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("type", "text");
            jsonBody.put("radio", mIntent.getStringExtra("radioId"));
            jsonBody.put("text", editText.getText());
            jsonBody.put("image","");
            jsonBody.put("active", false);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, getString(R.string.server)+"/api/console/ticker", jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if (response.getBoolean("success")){
                        Toast.makeText(SendTextBroadcast.this, "¡Mensaje enviado con éxito!", Toast.LENGTH_SHORT).show();
                        onBackPressed();

                    }else{
                        Toast.makeText(SendTextBroadcast.this, "Hubo algún problema con el servidor. Intente más tarde.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() {
                Realm realm = Realm.getInstance(SendTextBroadcast.this);
                RealmResults<UserRealm> user = realm.where(UserRealm.class).findAll();
                Map headers = new HashMap();
                headers.put("x-access-token", user.first().getToken());
                realm.close();
                return headers;

            }
        };

        rq.add(jor);

    }
}
