package com.vsantos.radios.gcm;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by vitor on 2/2/16.
 */
public class AppInstanceIDListenerService extends InstanceIDListenerService {

    @Override
    public void onTokenRefresh() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        preferences.edit().putBoolean("status", false).apply();

        Intent i = new Intent(this, RegistrationIntentService.class);
        startService(i);
    }
}
