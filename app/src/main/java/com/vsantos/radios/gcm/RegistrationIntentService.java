package com.vsantos.radios.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.vsantos.radios.R;
import com.vsantos.radios.realm.UserRealm;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by vitor on 2/2/16.
 */
public class RegistrationIntentService extends IntentService {
    public static final String LOG = "LOG";
    private RequestQueue rq;
    public RegistrationIntentService() {
        super(LOG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean status = preferences.getBoolean("status", false);

        synchronized (LOG){
            InstanceID instanceID = InstanceID.getInstance(this);
            try {
                //if(!status) {
                    String token = instanceID.getToken(getString(R.string.sender_id), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    preferences.edit().putBoolean("status", token != null && token.trim().length() > 0).apply();
                    rq = Volley.newRequestQueue(this);
                    sendRegistration(token);

                //}
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private void sendRegistration(String token) {
        Log.i("token ------>", token);
        final String url = getString(R.string.server) + "/api/add-token-push-notitification";


        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("token", token);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(LOG, response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {



            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Realm realm = Realm.getInstance(getApplicationContext());
                RealmResults<UserRealm> user = realm.where(UserRealm.class).findAll();

                Map headers = new HashMap();
                headers.put("x-access-token", user.first().getToken());

                realm.close();
                return headers;
            }

        };
        rq.add(jor);
    }
}
