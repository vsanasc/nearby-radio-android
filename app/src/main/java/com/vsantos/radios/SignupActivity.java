package com.vsantos.radios;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vitor on 12/30/15.
 */
public class SignupActivity extends AppCompatActivity {
    RequestQueue rq;
    Toolbar toolbar;
    EditText name, last,email, pass;
    Button register;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        rq = Volley.newRequestQueue(this);

        toolbar = (Toolbar) findViewById(R.id.toolBar);
        toolbar.setTitle("Registrarse");
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.mipmap.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        name = (EditText)findViewById(R.id.nameEdit);
        last = (EditText) findViewById(R.id.last_nameEdit);
        email = (EditText) findViewById(R.id.emailEdit);
        pass = (EditText)findViewById(R.id.passEdit);
        register = (Button)findViewById(R.id.btnSignup);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });


    }

    private void registerUser(){

        JSONObject body = new JSONObject();
        try {

            body.put("first_name", name.getText());
            body.put("last_name", last.getText());
            body.put("password", pass.getText());
            body.put("email", email.getText());
            body.put("type_user", "email");
            body.put("active", true);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, getString(R.string.server) + "/api/register", body, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    Toast.makeText(SignupActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                    if (response.getBoolean("success")){
                        onBackPressed();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(getClass().getSimpleName(), "error Volley");
            }
        });
        rq.add(jor);
    }
}
