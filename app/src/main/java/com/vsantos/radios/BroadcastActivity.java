package com.vsantos.radios;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.vsantos.radios.adapters.RecyclerTimelineAdapter;
import com.vsantos.radios.objects.TimelineObject;
import com.vsantos.radios.realm.UserRealm;
import com.vsantos.radios.utils.Utils;
import com.vsantos.radios.services.PlayerNotification;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


/**
 * Created by vitor on 12/22/15.
 */
public class BroadcastActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView recycler;
    RecyclerTimelineAdapter mAdapter;
    ArrayList<TimelineObject> list;
    RequestQueue rq;
    Socket mSocket;
    Intent mIntent;
    String channel;
    int scroll;
    int current_page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast);

        NearbyRadioApplication app = (NearbyRadioApplication) getApplication();

        //mSocket = app.getSocket();

        mIntent = getIntent();
        channel = "timeline-"+mIntent.getStringExtra("_id");

        toolbar = (Toolbar) findViewById(R.id.toolBar);
        toolbar.setTitle(mIntent.getStringExtra("name"));
        toolbar.setNavigationIcon(R.mipmap.ic_back_arrow);

        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Intent playRadio = new Intent(this, PlayerNotification.class);
        playRadio.setAction(Utils.ACTION.STARTFOREGROUND_ACTION);
        playRadio.putExtra("name", mIntent.getStringExtra("name"));
        playRadio.putExtra("url_stream", mIntent.getStringExtra("url_stream"));
        playRadio.putExtra("image_profile", mIntent.getStringExtra("image_profile"));
        startService(playRadio);


        FloatingActionButton btnFloat = (FloatingActionButton)findViewById(R.id.actionFloating);
        btnFloat.setImageDrawable(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_add).actionBar().color(Color.WHITE));
        btnFloat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(BroadcastActivity.this, SendTextBroadcast.class);
                it.putExtra("radioId", mIntent.getStringExtra("_id"));
                startActivity(it);
            }
        });


        list = new ArrayList<TimelineObject>();

        mAdapter = new RecyclerTimelineAdapter(this, list);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setReverseLayout(true);

        recycler = (RecyclerView)findViewById(R.id.timeline);
        recycler.setAdapter(mAdapter);
        recycler.setLayoutManager(linearLayoutManager);
        /*recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        */


        rq = Volley.newRequestQueue(this);
        Log.i(getClass().getSimpleName(), getString(R.string.server) + "/api/ticker/" + mIntent.getStringExtra("_id"));
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, getString(R.string.server) + "/api/ticker/"+mIntent.getStringExtra("_id"), (String) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(getClass().getSimpleName(), response.toString());
                try {
                    JSONArray results = response.getJSONArray("results");
                    for (int i=0;i<results.length();i++){

                        TimelineObject elem = new TimelineObject(results.getJSONObject(i));
                        list.add(elem);

                    }
                    mAdapter.notifyItemInserted(list.size());
                    recycler.smoothScrollToPosition(0);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() {
                Realm realm = Realm.getInstance(BroadcastActivity.this);
                RealmResults<UserRealm> user = realm.where(UserRealm.class).findAll();
                Map headers = new HashMap();
                headers.put("x-access-token", user.first().getToken());
                realm.close();
                return headers;

            }
        };

        rq.add(jor);

        mSocket.emit("subscribe", channel);
        mSocket.on(channel, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                final JSONObject data = (JSONObject) args[0];
                play_sound_notification();
                Log.i("data Socket -->", data.toString());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            TimelineObject elem = new TimelineObject(data);

                            list.add(0, elem);
                            mAdapter.notifyDataSetChanged();
                            recycler.smoothScrollToPosition(0);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });

            }
        });
        mSocket.connect();




    }
    private void play_sound_notification(){
        Notification.Builder noti = new Notification.Builder(this)
                .setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.new_ticker_alert));

        NotificationManager nm = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(2323223, noti.build());


    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.emit("unsubscribe", channel);
        mSocket.off(channel);

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

}
