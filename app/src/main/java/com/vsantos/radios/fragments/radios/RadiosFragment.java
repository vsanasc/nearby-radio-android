package com.vsantos.radios.fragments.radios;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vsantos.radios.R;
import com.vsantos.radios.adapters.RadioViewPagerAdapter;
import com.vsantos.radios.utils.SlidingTabLayout;

/**
 * Created by vitor on 12/19/15.
 */
public class RadiosFragment extends Fragment {
    SlidingTabLayout tabs;
    ViewPager viewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_radios, container, false);

        viewPager = (ViewPager) v.findViewById(R.id.radioPagers);
        viewPager.setAdapter(new RadioViewPagerAdapter(getFragmentManager(), getContext()));

        tabs = (SlidingTabLayout) v.findViewById(R.id.tabs);
        tabs.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        tabs.setViewPager(viewPager);

        return v;
    }
}
