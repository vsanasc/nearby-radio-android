package com.vsantos.radios.fragments.programs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vsantos.radios.R;

/**
 * Created by vitor on 12/21/15.
 */
public class ProgramDetailInfoFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_info, container, false);

        return v;
    }
    @Override
    public void onResume(){
        super.onResume();

    }
}
