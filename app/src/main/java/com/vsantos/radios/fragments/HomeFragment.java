package com.vsantos.radios.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vsantos.radios.R;
import com.vsantos.radios.adapters.RecyclerPostAdapter;
import com.vsantos.radios.fragments.programs.ProgramsFragment;
import com.vsantos.radios.fragments.radios.RadiosFragment;
import com.vsantos.radios.objects.PostObject;
import com.vsantos.radios.realm.UserRealm;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by vitor on 12/19/15.
 */
public class HomeFragment extends Fragment {
    private RequestQueue rq;
    private String TAG = getClass().getSimpleName();
    private ArrayList<PostObject> list;
    private RecyclerView recycler;
    private Realm realm;
    private RealmResults<UserRealm> user;
    private View v;
    private RelativeLayout alertAnonymus;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rq = Volley.newRequestQueue(getContext());
        list = new ArrayList<PostObject>();

        v = inflater.inflate(R.layout.fragment_home, container, false);

        alertAnonymus = (RelativeLayout)v.findViewById(R.id.alertAnonymus);

        recycler = (RecyclerView) v.findViewById(R.id.listPosts);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,false));

        String url = getString(R.string.server)+ "/api/posts/home";

        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (String)null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, response.toString());
                try {
                    
                    JSONArray listResponse = response.getJSONArray("results");

                    for (int i=0;i<listResponse.length(); i++) {

                        JSONObject elem = listResponse.getJSONObject(i);

                        PostObject obj = new PostObject(elem);

                        list.add(obj);

                    }
                    recycler.setAdapter(new RecyclerPostAdapter(getContext(), list));

                    Realm realm = Realm.getInstance(getContext());
                    RealmResults<UserRealm> userRealmResults = realm.where(UserRealm.class).findAll();

                    if(userRealmResults.size() == 0){
                        alertAnonymus.setVisibility(View.VISIBLE);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getContext(), error.getMessage(),Toast.LENGTH_SHORT).show();
                //Log.e("Error Volley",error.getMessage());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                realm = Realm.getInstance(getContext());
                user = realm.where(UserRealm.class).findAll();
                Map headers = new HashMap();

                if(user.size() > 0) {
                    headers.put("x-access-token", user.first().getToken());
                }
                realm.close();
                return headers;
            }
        };
        rq.add(jor);


        return v;
    }


}
