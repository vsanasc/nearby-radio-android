package com.vsantos.radios.fragments.programs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vsantos.radios.ProgramDetailActivity;
import com.vsantos.radios.R;
import com.vsantos.radios.adapters.RecyclerPostAdapter;
import com.vsantos.radios.objects.PostObject;
import com.vsantos.radios.objects.ProgramObject;
import com.vsantos.radios.realm.UserRealm;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by vitor on 1/6/16.
 */
public class ProgramDetailPublicationsFragment extends Fragment {
    private String TAG = getClass().getSimpleName();
    private RequestQueue rq;
    private ProgramObject program;
    private ArrayList<PostObject> list;
    private RecyclerView recycler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        program = ProgramDetailActivity.program;
        rq = Volley.newRequestQueue(getContext());
        list = new ArrayList<PostObject>();
        String uuid = getActivity().getIntent().getStringExtra("_id");

        View v = inflater.inflate(R.layout.fragment_posts, container, false);

        recycler = (RecyclerView) v.findViewById(R.id.listPosts);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,false));

        String url = getString(R.string.server)+ "/api/posts/?program="+ uuid;

        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (String)null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, response.toString());
                try {
                    JSONArray listResponse = response.getJSONArray("results");

                    for (int i=0;i<listResponse.length(); i++) {

                        JSONObject elem = listResponse.getJSONObject(i);
                        PostObject obj = new PostObject(elem);


                        list.add(obj);

                    }

                    recycler.setAdapter(new RecyclerPostAdapter(getContext(), list));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getContext(), error.getMessage(),Toast.LENGTH_SHORT).show();
                //Log.e("Error Volley",error.getMessage());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Realm realm = Realm.getInstance(getContext());
                RealmResults<UserRealm> user = realm.where(UserRealm.class).findAll();

                Map headers = new HashMap();
                headers.put("x-access-token", user.first().getToken());
                realm.close();
                return headers;
            }
        };

        rq.add(jor);


        return v;
    }
}
