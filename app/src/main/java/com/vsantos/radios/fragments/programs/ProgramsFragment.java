package com.vsantos.radios.fragments.programs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vsantos.radios.R;
import com.vsantos.radios.adapters.ProgramViewPagerAdapter;
import com.vsantos.radios.utils.SlidingTabLayout;

/**
 * Created by vitor on 12/27/15.
 */
public class ProgramsFragment extends Fragment {
    SlidingTabLayout tabs;
    ViewPager viewPager;
    LayoutInflater inflater;
    ProgramViewPagerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_programs, container, false);

        adapter = new ProgramViewPagerAdapter(getFragmentManager(), getContext());

        viewPager = (ViewPager) v.findViewById(R.id.programPagers);
        viewPager.setAdapter(adapter);

        tabs = (SlidingTabLayout) v.findViewById(R.id.tabs);
        tabs.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        tabs.setViewPager(viewPager);

        return v;
    }


}
