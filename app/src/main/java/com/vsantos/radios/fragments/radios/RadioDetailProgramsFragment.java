package com.vsantos.radios.fragments.radios;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vsantos.radios.R;
import com.vsantos.radios.adapters.RecyclerProgramsAdapter;
import com.vsantos.radios.objects.ProgramObject;
import com.vsantos.radios.realm.UserRealm;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by vitor on 1/6/16.
 */
public class RadioDetailProgramsFragment extends Fragment {
    private RequestQueue rq;
    private String TAG = getClass().getSimpleName();
    private ArrayList<ProgramObject> list;
    private RecyclerView recycler;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rq = Volley.newRequestQueue(getContext());
        list = new ArrayList<ProgramObject>();

        View v = inflater.inflate(R.layout.fragment_grid_list, container, false);

        recycler = (RecyclerView)v.findViewById(R.id.list);
        recycler.setHasFixedSize(true);

        if(getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            recycler.setLayoutManager(new GridLayoutManager(getContext(), 2));
        }
        else{
            recycler.setLayoutManager(new GridLayoutManager(getContext(), 4));
        }
        String url = getString(R.string.server)+"/api/programs/?radio="+ getActivity().getIntent().getStringExtra("_id");
        Log.i(getClass().getSimpleName(), url);
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (String) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.i(TAG, response.toString());
                try {
                    JSONArray listResponse = response.getJSONArray("results");

                    for (int i=0;i<listResponse.length(); i++) {

                        JSONObject elem = listResponse.getJSONObject(i);
                        ProgramObject obj = new ProgramObject(elem);

                        list.add(obj);

                    }

                    recycler.setAdapter(new RecyclerProgramsAdapter(getContext(), list, true));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Realm realm = Realm.getInstance(getContext());
                RealmResults<UserRealm> user = realm.where(UserRealm.class).findAll();

                HashMap headers = new HashMap();
                headers.put("x-access-token", user.first().getToken());

                realm.close();
                return headers;
            }
        };

        rq.add(jor);




        return v;
    }
}
