package com.vsantos.radios.fragments.radios;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vsantos.radios.R;

/**
 * Created by vitor on 12/21/15.
 */
public class RadioDetailInfoFragment extends Fragment {
    TextView info;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_info, container, false);

        info = (TextView) v.findViewById(R.id.infoDetail);

        return v;
    }
    @Override
    public void onResume(){
        super.onResume();

    }
}
