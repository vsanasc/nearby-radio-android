package com.vsantos.radios;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.squareup.picasso.Picasso;
import com.vsantos.radios.adapters.RadioDetailViewPagerAdapter;
import com.vsantos.radios.objects.RadioObject;
import com.vsantos.radios.realm.UserRealm;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;


public class RadioDetailActivity extends AppCompatActivity {
    RequestQueue rq;
    CollapsingToolbarLayout toolbarLayout;
    Toolbar toolbar;
    ImageView imageHeader;
    TabLayout tabs;
    ViewPager viewPager;
    Context ctx;
    Intent i;
    public static RadioObject radio;
    final String TAG = getClass().getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_detail);
        rq = Volley.newRequestQueue(this);
        i = getIntent();
        ctx = this;


        loadRadio();

        toolbar = (Toolbar) findViewById(R.id.toolBar);

        toolbar.setTitle(i.getStringExtra("title"));

        toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingLayout);

        tabs = (TabLayout) findViewById(R.id.tabs);


        toolbar.setNavigationIcon(R.mipmap.ic_back_arrow);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        viewPager = (ViewPager) findViewById(R.id.viewPagerRadioDetail);
        viewPager.setAdapter(new RadioDetailViewPagerAdapter(getSupportFragmentManager(), this, i.getStringExtra("_id")));
        viewPager.setCurrentItem(1);

        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        tabs.setTabMode(TabLayout.MODE_SCROLLABLE);

        imageHeader = (ImageView) findViewById(R.id.imageHeader);
        Picasso.with(this).load(i.getStringExtra("image_detail")).into(imageHeader);



    }
    public void loadRadio(){

        String url = getString(R.string.server)+"/api/radios/"+ i.getStringExtra("_id");

        Log.i(TAG, url);

        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (String)null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    radio = new RadioObject(response);
                    TextView info = (TextView) findViewById(R.id.infoDetail);
                    info.setText(radio.getInfo());
                    toolbarLayout.setContentScrimColor(Color.parseColor(radio.getBg_color()));
                    tabs.setBackgroundColor(Color.parseColor(radio.getBg_color()));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Realm realm = Realm.getInstance(RadioDetailActivity.this);
                RealmResults<UserRealm> user = realm.where(UserRealm.class).findAll();

                Map headers = new HashMap();
                headers.put("x-access-token", user.first().getToken());

                realm.close();
                return headers;
            }
        };

        rq.add(jor);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.radio_menu, menu);
        menu.findItem(R.id.action_follow).setIcon(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_star).actionBar().color(Color.WHITE));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_streaming:
                Intent i = new Intent(RadioDetailActivity.this, BroadcastActivity.class);
                i.putExtra("_id", radio.getId());
                i.putExtra("name", radio.getName());
                i.putExtra("image_profile", radio.getImage_profile());
                i.putExtra("url_stream", radio.getUrl_streaming());
                startActivity(i);
                return true;


            case R.id.action_follow:
                followAction();
                return true;


            default:
                return super.onOptionsItemSelected(item);

        }

    }

    private void followAction(){

        JSONObject data = null;
        try {
            data = new JSONObject();
            data.put("radioId", radio.getId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = getString(R.string.server) + "/api/follow";

        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, url, data, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                String msg = null;

                try {

                    msg = (response.getBoolean("follow"))?getString(R.string.follow_toast_msg):getString(R.string.unfollow_toast_msg);

                    radio.setUser_follow(response.getBoolean("follow"));
                    Toast.makeText(ctx, msg + radio.getName(), Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Realm realm = Realm.getInstance(RadioDetailActivity.this);
                RealmResults<UserRealm> user = realm.where(UserRealm.class).findAll();

                Map headers = new HashMap();
                headers.put("x-access-token", user.first().getToken());
                return headers;
            }
        };
        rq.add(jor);

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }
}
