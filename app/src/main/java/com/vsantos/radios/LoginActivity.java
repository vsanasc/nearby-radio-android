package com.vsantos.radios;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.vsantos.radios.realm.UserRealm;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import io.realm.Realm;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private static final int SIGN_IN_COGE = 3453;
    RequestQueue rq;
    Toolbar toolbar;
    EditText email, password;
    TextView btnRegister, btnForgot;
    CallbackManager callbackManager;
    Button loginFacebook, loginGoogle, loginEmail;
    Realm realm;
    GoogleApiClient mGoogleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        rq = Volley.newRequestQueue(this);
        realm = Realm.getInstance(this);

        email = (EditText)findViewById(R.id.emailEdit);
        password = (EditText)findViewById(R.id.passEdit);

        btnRegister = (TextView)findViewById(R.id.btnSignup);
        btnForgot = (TextView)findViewById(R.id.btnForgot);

        loginFacebook = (Button) findViewById(R.id.loginFacebook);
        loginGoogle = (Button) findViewById(R.id.loginGoogle);
        loginEmail = (Button) findViewById(R.id.btnLoginEmail);

        toolbar = (Toolbar)findViewById(R.id.toolBar);
        toolbar.setTitle("Login");
        toolbar.setNavigationIcon(R.mipmap.ic_back_arrow);

        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        loginFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        final GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {


                                JSONObject jsonBody = null;

                                try {
                                    jsonBody = new JSONObject();
                                    jsonBody.put("platform", "android");
                                    jsonBody.put("token_device", "");
                                    jsonBody.put("first_name", object.getString("first_name"));
                                    jsonBody.put("last_name", object.getString("last_name"));
                                    jsonBody.put("id_facebook", object.getString("id"));
                                    jsonBody.put("gender", object.getString("gender"));
                                    jsonBody.put("age_range", object.getJSONObject("age_range").toString());
                                    jsonBody.put("type_user", "facebook");
                                    jsonBody.put("active", true);


                                    if (jsonBody.has("birthday")) {
                                        jsonBody.put("birthday", object.getString("birthday"));
                                    } else {
                                        jsonBody.put("birthday", "");
                                    }

                                    if (object.has("email")) {
                                        jsonBody.put("email", object.getString("email"));
                                    } else {
                                        jsonBody.put("email", "");
                                    }


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                final JSONObject dataFinal = jsonBody;

                                JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, getString(R.string.server) + "/api/authenticate", jsonBody, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {

                                        try {
                                            if (response.getBoolean("success")) {
                                                setUser(response);
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                    }
                                });

                                rq.add(jor);
                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "email, id, name, first_name, last_name, gender, birthday, age_range");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(LoginActivity.this, "Error:" + error.toString(), Toast.LENGTH_LONG).show();
                    }
                });
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "email"));
            }
        });

        loginGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, SIGN_IN_COGE);
            }
        });

        loginEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONObject body = new JSONObject();
                try {

                    body.put("email", email.getText());
                    body.put("password", password.getText());
                    body.put("type_user", "email");

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, getString(R.string.server) + "/api/authenticate", body,new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            if(response.getBoolean("success")){
                                setUser(response);
                            }else{
                                Toast.makeText(LoginActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
                rq.add(jor);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(i);
            }
        });

        btnForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, ForgotPassActivity.class);
                startActivity(i);
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SIGN_IN_COGE){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }else {
            //Login facebook
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            if (acct != null) {
                JSONObject jsonBody = null;

                try {

                    jsonBody = new JSONObject();
                    jsonBody.put("platform", "android");
                    jsonBody.put("token_device", "");
                    jsonBody.put("first_name", acct.getDisplayName());
                    jsonBody.put("last_name", "");
                    jsonBody.put("image", acct.getPhotoUrl());
                    jsonBody.put("id_google", acct.getId());
                    jsonBody.put("type_user", "google");
                    jsonBody.put("active", true);
                    jsonBody.put("email", acct.getEmail());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, getString(R.string.server) + "/api/authenticate", jsonBody, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            if(response.getBoolean("success")){
                                setUser(response);
                            }else{
                                Toast.makeText(LoginActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
                rq.add(jor);


            }
        }
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks whether a hardware keyboard is available
        if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
            Toast.makeText(this, "keyboard visible", Toast.LENGTH_SHORT).show();
        } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
            Toast.makeText(this, "keyboard hidden", Toast.LENGTH_SHORT).show();
        }
    }
    private void setUser(JSONObject response){
        try {


            JSONObject user_response = response.getJSONObject("result");

            UserRealm user = new UserRealm();
            user.setFirst_name(user_response.getString("first_name"));
            user.setLast_name(user_response.getString("last_name"));
            user.setEmail(user_response.getString("email"));
            user.setType_user(user_response.getString("type_user"));
            user.setToken(response.getString("token"));
            user.setImage(user_response.getString("image"));
            user.setId_server(user_response.getString("_id"));


            realm.beginTransaction();
            realm.copyToRealm(user);
            realm.commitTransaction();

            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        mGoogleApiClient.connect();
    }
}
