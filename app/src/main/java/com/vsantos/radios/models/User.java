package com.vsantos.radios.models;

//import io.realm.RealmObject;
//import io.realm.annotations.PrimaryKey;

/**
 * Created by vitor on 12/29/15.
 */
public class User{

    //@PrimaryKey
    private String ObjectId;

    private String first_name;
    private String last_name;
    private String email;
    private String token;

    private String token_push_notification;
    private Integer age;
    private Integer type;
    private Boolean active;


    public String getToken_push_notification() {
        return token_push_notification;
    }

    public String getObjectId() {
        return ObjectId;
    }

    public void setObjectId(String objectId) {
        ObjectId = objectId;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
