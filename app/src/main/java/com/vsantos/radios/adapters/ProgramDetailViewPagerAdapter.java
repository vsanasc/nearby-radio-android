package com.vsantos.radios.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.vsantos.radios.fragments.programs.ProgramDetailInfoFragment;
import com.vsantos.radios.fragments.programs.ProgramDetailPublicationsFragment;

/**
 * Created by vitor on 12/21/15.
 */
public class ProgramDetailViewPagerAdapter extends FragmentStatePagerAdapter {
    private Context ctx;
    private String objectId;
    private String[] titles = {"Información","Publicaciones"};

    public ProgramDetailViewPagerAdapter(FragmentManager fm, Context ctx, String objectId) {
        super(fm);
        this.ctx = ctx;
        this.objectId = objectId;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;

        switch (position){

            case 0:
                frag = new ProgramDetailInfoFragment();
                break;

            case 1:
                frag = new ProgramDetailPublicationsFragment();
                break;
        }

        Bundle params = new Bundle();
        params.putString("objectIdProgram", this.objectId);

        if (frag != null) {
            frag.setArguments(params);
        }

        return frag;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
