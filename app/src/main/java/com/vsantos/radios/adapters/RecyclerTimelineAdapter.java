package com.vsantos.radios.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.vsantos.radios.R;
import com.vsantos.radios.objects.TimelineObject;

import java.util.ArrayList;

/**
 * Created by vitor on 2/4/16.
 */
public class RecyclerTimelineAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context ctx;
    ArrayList<TimelineObject> list;
    LayoutInflater layoutInflater;

    public RecyclerTimelineAdapter(Context ctx, ArrayList<TimelineObject> list) {
        this.ctx = ctx;
        this.list = list;
        this.layoutInflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getItemCount() {
        return list.size();
    }
    @Override
    public int getItemViewType(int position) {

        switch (list.get(position).getType()){
            case "start":
                return 1;

            case "text":
                return 2;

            case "image":
                return 3;

            case "youtube":
                return 4;

            default:
                return 0;
        }


    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case 1:
                View view = layoutInflater.inflate(R.layout.item_timeline_start, parent, false);;
                return new StartViewHolder(view);


            default:
                view = layoutInflater.inflate(R.layout.item_timeline, parent, false);
                return new MainViewHolder(view);

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TimelineObject elem = list.get(position);

        if(!elem.getType().equals("start")) {

            MainViewHolder vh = (MainViewHolder)holder;

            vh.profileName.setText(elem.getNameProfile());
            vh.text.setText(elem.getText());

            Picasso.with(ctx).load(elem.getImageProfile()).memoryPolicy(MemoryPolicy.NO_STORE).into(vh.profileImage);

            if (!elem.getImageContent().equals("")) {
                Picasso.with(ctx).load(elem.getImageContent()).memoryPolicy(MemoryPolicy.NO_CACHE).into(vh.imageContent);
            } else {
                vh.imageContent.setVisibility(View.GONE);
            }

            loadIcon(elem, vh.icon, vh.bgIcon);

        }else{
            StartViewHolder vh = (StartViewHolder)holder;

            vh.titleTransmition.setText(elem.getText());
            Picasso.with(ctx).load(elem.getImageProfile()).into(vh.imageProgram);

        }

    }

    private void loadIcon(TimelineObject elem, ImageView icon, LinearLayout bgIcon) {

        switch (elem.getType()){
            case "youtube":
                icon.setImageResource(R.mipmap.ic_youtube);
                bgIcon.setBackgroundResource(R.drawable.circle_timeline_red);
                break;

            case "text":
                icon.setImageResource(R.mipmap.ic_message);
                bgIcon.setBackgroundResource(R.drawable.circle_timeline_yellow);
                break;

            case "image":
                icon.setImageResource(R.mipmap.ic_image);
                bgIcon.setBackgroundResource(R.drawable.circle_timeline_green);
                break;

        }

    }
    public class MainViewHolder extends RecyclerView.ViewHolder {
        ImageView icon, profileImage, imageContent;
        TextView profileName, text;
        LinearLayout bgIcon;

        public MainViewHolder(View itemView) {
            super(itemView);

            icon = (ImageView) itemView.findViewById(R.id.iconTimeline);
            profileImage = (ImageView) itemView.findViewById(R.id.profileImage);
            imageContent = (ImageView) itemView.findViewById(R.id.imageTimeline);

            profileName = (TextView) itemView.findViewById(R.id.profileName);
            text = (TextView) itemView.findViewById(R.id.textTimeline);

            bgIcon = (LinearLayout)itemView.findViewById(R.id.lineType);

            imageContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(ctx, "Image content clicked", Toast.LENGTH_SHORT).show();
                }
            });

            profileName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(ctx, "name clicked", Toast.LENGTH_SHORT).show();
                }
            });


        }
    }
    public class StartViewHolder extends RecyclerView.ViewHolder{
        ImageView imageProgram;
        TextView titleTransmition;
        public StartViewHolder(View itemView) {
            super(itemView);

            imageProgram = (ImageView)itemView.findViewById(R.id.imageProgram);
            titleTransmition = (TextView)itemView.findViewById(R.id.titleTransmition);

        }
    }
}
