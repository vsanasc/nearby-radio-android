package com.vsantos.radios.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.squareup.picasso.Picasso;
import com.vsantos.radios.R;
import com.vsantos.radios.objects.PostObject;
import com.vsantos.radios.objects.TimelineObject;

import java.util.ArrayList;

/**
 * Created by vitor on 1/10/16.
 */
public class RecyclerPostAdapter extends RecyclerView.Adapter<RecyclerPostAdapter.MyViewHolder>{
    private Context ctx;
    private ArrayList<PostObject> list;
    private LayoutInflater layoutInflater;

    public RecyclerPostAdapter(Context context, ArrayList<PostObject> list) {
        this.ctx = context;
        this.list = list;
        layoutInflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public int getItemViewType(int position) {

        switch (list.get(position).getType()){

            case "post":
                return 0;

            case "rrss":
                return 1;

            case "image":
                return 2;

            case "youtube":
                return 3;

            case "twitter":
                return 4;

            case "facebook":
                return 5;

            case "instragram":
                return 6;

            default:
                return 0;
        }


    }

    @Override
    public RecyclerPostAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.item_list_post, parent, false);

        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerPostAdapter.MyViewHolder holder, int position) {
        PostObject elem = list.get(position);
        holder.textView.setText(elem.getText());
        holder.nameProfile.setText(elem.getName_profile());
        holder.typeIcon.bringToFront();
        loadIcon(elem, holder.typeIcon);
        holder.time.setReferenceTime(elem.getDate().getTime());
        Picasso.with(ctx).load(elem.getImage()).into(holder.imageView);
        Picasso.with(ctx).load(elem.getImage_profile()).placeholder(R.mipmap.default_profile).error(R.mipmap.default_profile).into(holder.profileImage);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void loadIcon(PostObject elem, ImageView icon) {

        switch (elem.getType()){

            case "post":
                icon.setImageResource(R.mipmap.ic_rrss);
                break;

            case "image":
                icon.setImageResource(R.mipmap.ic_image);
                break;

            case "twitter":
                icon.setImageResource(R.mipmap.ic_twitter);
                break;

            case "facebook":
                icon.setImageResource(R.mipmap.ic_facebook);
                break;

            case "youtube":
                icon.setImageResource(R.mipmap.ic_youtube);
                break;

            case "instagram":
                icon.setImageResource(R.mipmap.ic_instagram);
                break;

        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView, nameProfile;
        ImageView imageView, profileImage, typeIcon;
        RelativeTimeTextView time;

        public MyViewHolder(View itemView) {
            super(itemView);

            textView = (TextView) itemView.findViewById(R.id.textPost);
            nameProfile = (TextView)itemView.findViewById(R.id.nameProfile);
            imageView = (ImageView) itemView.findViewById(R.id.imagePost);
            typeIcon = (ImageView) itemView.findViewById(R.id.typeIcon);
            profileImage = (ImageView) itemView.findViewById(R.id.profileImage);
            time = (RelativeTimeTextView)itemView.findViewById(R.id.time);

        }
    }
}
