package com.vsantos.radios.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vsantos.radios.R;
import com.vsantos.radios.objects.BroadcasterObject;

import java.util.ArrayList;

/**
 * Created by vitor on 12/22/15.
 */
public class RecyclerBroadcasterAdapter extends RecyclerView.Adapter<RecyclerBroadcasterAdapter.MyViewHolder> {
    private Context ctx;
    private LayoutInflater layoutInflater;
    private ArrayList<BroadcasterObject> list;

    public RecyclerBroadcasterAdapter(Context ctx, ArrayList<BroadcasterObject> data) {
        this.ctx = ctx;
        this.list = data;
        this.layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.item_list_broadcaster, parent, false);

        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.name.setText(list.get(position).getName());
        holder.description.setText(list.get(position).getDescription());
        Picasso.with(ctx).load(list.get(position).getImage()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name, description;
        public MyViewHolder(View itemView) {
            super(itemView);

            image = (ImageView)itemView.findViewById(R.id.imageBroadcaster);
            name = (TextView)itemView.findViewById(R.id.nameBroadcaster);
            description = (TextView)itemView.findViewById(R.id.descriptionBroadcaster);

        }
    }
}
