package com.vsantos.radios.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.vsantos.radios.ProgramDetailActivity;
import com.vsantos.radios.R;
import com.vsantos.radios.objects.ProgramObject;
import com.vsantos.radios.realm.UserRealm;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by vitor on 12/19/15.
 */
public class RecyclerProgramsAdapter extends RecyclerView.Adapter<RecyclerProgramsAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    private Context ctx;
    private ArrayList<ProgramObject> list;
    private final String TAG = getClass().getSimpleName();
    private Boolean global_list;

    public RecyclerProgramsAdapter(Context ctx, ArrayList<ProgramObject> response, Boolean global_list) {
        this.ctx = ctx;
        this.inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = response;
        this.global_list = global_list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = inflater.inflate(R.layout.item_list_radio, parent, false);

        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ProgramObject elem = list.get(position);

        holder.name.setText(elem.getName());
        holder.name.setTextColor(Color.parseColor(elem.getText_color()));
        holder.block.setBackgroundColor(Color.parseColor(elem.getBg_color()));
        holder.topic.setText(elem.getTopic());

        if(list.get(position).getUser_follow()){
            holder.follow.setText("SIGUIENDO");
        }else{
            holder.follow.setText("SEGUIR");
        }
        Picasso.with(ctx).load(list.get(position).getImage_list()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RequestQueue rq;
        RelativeLayout block;
        ImageView image;
        TextView name, follow, topic;
        public MyViewHolder(View itemView) {
            super(itemView);

            rq = Volley.newRequestQueue(ctx);

            block = (RelativeLayout) itemView.findViewById(R.id.block);
            image = (ImageView)itemView.findViewById(R.id.imageTop);
            name = (TextView) itemView.findViewById(R.id.nameRadio);
            topic = (TextView)itemView.findViewById(R.id.info_extra);
            follow = (TextView) itemView.findViewById(R.id.follow);

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx, ProgramDetailActivity.class);
                    i.putExtra("_id", list.get(getAdapterPosition()).getId());
                    i.putExtra("title", list.get(getAdapterPosition()).getName());
                    i.putExtra("image", list.get(getAdapterPosition()).getImage_list());
                    i.putExtra("bg_color", list.get(getAdapterPosition()).getBg_color());
                    ctx.startActivity(i);

                }
            });

            follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    JSONObject data = null;
                    try {
                        data = new JSONObject();
                        data.put("programId", list.get(getAdapterPosition()).getId());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String url = ctx.getString(R.string.server)+"/api/follow";

                    JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, url, data, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            String msg = null;

                            try {

                                list.get(getAdapterPosition()).setUser_follow(response.getBoolean("follow"));

                                if(response.getBoolean("follow")){
                                    msg = ctx.getString(R.string.follow_toast_msg);
                                    follow.setText(R.string.follow_block_text);
                                }else{
                                    msg = ctx.getString(R.string.unfollow_toast_msg);
                                    follow.setText(R.string.unfollow_block_text);
                                }

                                Toast.makeText(ctx, msg+list.get(getAdapterPosition()).getName(), Toast.LENGTH_SHORT).show();
                                if(!global_list){
                                    list.remove(getAdapterPosition());
                                    notifyDataSetChanged();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }){
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Realm realm = Realm.getInstance(ctx);
                            RealmResults<UserRealm> user = realm.where(UserRealm.class).findAll();

                            Map headers = new HashMap();
                            headers.put("x-access-token", user.first().getToken());
                            realm.close();
                            return headers;
                        }
                    };
                    rq.add(jor);

                }
            });

        }

        @Override
        public void onClick(View v) {

        }
    }
}
