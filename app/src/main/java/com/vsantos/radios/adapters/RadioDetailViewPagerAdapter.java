package com.vsantos.radios.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.vsantos.radios.fragments.radios.RadioDetailProgramsFragment;
import com.vsantos.radios.fragments.radios.RadioDetailPublicationsFragment;
import com.vsantos.radios.fragments.radios.RadioDetailInfoFragment;

/**
 * Created by vitor on 12/21/15.
 */
public class RadioDetailViewPagerAdapter extends FragmentStatePagerAdapter {
    private Context ctx;
    private String objectId;
    private String[] titles = {"Información","Publicaciones", "Programas"};

    public RadioDetailViewPagerAdapter(FragmentManager fm, Context ctx, String objectId) {
        super(fm);
        this.ctx = ctx;
        this.objectId = objectId;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;

        switch (position){
            case 0:
                frag = new RadioDetailInfoFragment();
                break;

            case 1:
                frag = new RadioDetailPublicationsFragment();
                break;

            case 2:
                frag = new RadioDetailProgramsFragment();
                break;

        }

        Bundle params = new Bundle();
        params.putString("objectIdRadio", this.objectId);

        if (frag != null) {
            frag.setArguments(params);
        }

        return frag;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
