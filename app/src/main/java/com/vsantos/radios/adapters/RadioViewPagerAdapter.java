package com.vsantos.radios.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.vsantos.radios.fragments.radios.RadiosAllFragment;

/**
 * Created by vitor on 12/19/15.
 */
public class RadioViewPagerAdapter extends FragmentStatePagerAdapter {
    private Context ctx;
    private String[] titles = {"Todas", "Mis radios"};

    public RadioViewPagerAdapter(FragmentManager fm, Context c) {
        super(fm);
        ctx = c;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        switch (position){
            case 0:
                frag = new RadiosAllFragment();
                bundle.putString("type", "all");
                break;

            case 1:
                frag = new RadiosAllFragment();
                bundle.putString("type", "my");
                break;
        }

        if (frag != null) {

            frag.setArguments(bundle);
        }

        return frag;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }
}
