package com.vsantos.radios.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.vsantos.radios.R;
import com.vsantos.radios.objects.NotificationObject;
import com.vsantos.radios.realm.UserRealm;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by vitor on 3/5/16.
 */
public class RecyclerNotificationAdapter extends RecyclerView.Adapter<RecyclerNotificationAdapter.MyViewHolder>{
    ArrayList<NotificationObject> list;
    Context ctx;
    LayoutInflater layoutInflater;
    RequestQueue rq;

    public RecyclerNotificationAdapter(Context ctx, ArrayList<NotificationObject> list) {
        this.rq = Volley.newRequestQueue(ctx);
        this.ctx = ctx;
        this.list = list;
        this.layoutInflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_notification, parent, false);

        MyViewHolder vh = new MyViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerNotificationAdapter.MyViewHolder holder, int position) {
        NotificationObject elem = list.get(position);

        holder.name.setText(elem.getName());
        Picasso.with(ctx).load(elem.getImage()).into(holder.image);

        for (int i=0;i<10;i++){
            try {

                switch (i){
                    case 0:
                        if(elem.getConfigs().length() >= (i+1)){
                            JSONObject checkboxData = elem.getConfigs().getJSONObject(i);
                            holder.cb1.setText(checkboxData.getString("label"));
                            holder.cb1.setChecked(checkboxData.getBoolean("value"));
                            holder.cb1.setOnCheckedChangeListener(changeConfig(checkboxData));
                        }else{
                            holder.cb1.setVisibility(View.GONE);
                        }
                        break;

                    case 1:
                        if(elem.getConfigs().length() >= (i+1)){
                            JSONObject checkboxData = elem.getConfigs().getJSONObject(i);
                            holder.cb2.setText(checkboxData.getString("label"));
                            holder.cb2.setChecked(checkboxData.getBoolean("value"));
                        }else{
                            holder.cb2.setVisibility(View.GONE);
                        }
                        break;

                    case 2:
                        holder.cb3.setVisibility(View.GONE);
                        break;

                    case 3:
                        holder.cb4.setVisibility(View.GONE);
                        break;

                    case 4:
                        holder.cb5.setVisibility(View.GONE);
                        break;

                    case 5:
                        holder.cb6.setVisibility(View.GONE);
                        break;

                    case 6:
                        holder.cb7.setVisibility(View.GONE);
                        break;

                    case 7:
                        holder.cb8.setVisibility(View.GONE);
                        break;

                    case 8:
                        holder.cb9.setVisibility(View.GONE);
                        break;

                    case 9:
                        holder.cb10.setVisibility(View.GONE);
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name;
        CheckBox cb1,cb2,cb3,cb4,cb5,cb6,cb7,cb8,cb9,cb10;
        public MyViewHolder(View itemView) {
            super(itemView);

            name = (TextView)itemView.findViewById(R.id.name);
            image = (ImageView)itemView.findViewById(R.id.image);

            //Checkboxs
            cb1 = (CheckBox)itemView.findViewById(R.id.checkBox1);
            cb2 = (CheckBox)itemView.findViewById(R.id.checkBox2);
            cb3 = (CheckBox)itemView.findViewById(R.id.checkBox3);
            cb4 = (CheckBox)itemView.findViewById(R.id.checkBox4);
            cb5 = (CheckBox)itemView.findViewById(R.id.checkBox5);
            cb6 = (CheckBox)itemView.findViewById(R.id.checkBox6);
            cb7 = (CheckBox)itemView.findViewById(R.id.checkBox7);
            cb8 = (CheckBox)itemView.findViewById(R.id.checkBox8);
            cb9 = (CheckBox)itemView.findViewById(R.id.checkBox9);
            cb10 = (CheckBox)itemView.findViewById(R.id.checkBox10);


        }
    }

    private CompoundButton.OnCheckedChangeListener changeConfig(final JSONObject elem){

        return new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                JSONObject body = new JSONObject();
                try {
                    body.put("key", elem.getString("key"));
                    body.put("value", isChecked);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JsonObjectRequest jor = new JsonObjectRequest(Request.Method.PUT, ctx.getString(R.string.server) + "/api/update-notification-config/fHWdklH1ynE:APA91bE9lG0TUXmGWEYLyteKt3lRv5DZxqlHgys8iOFRVyBsn6AmDoPxWgA-nWEwyPDT5q9fTgZKzAbtzk_SgCdroSy-Di1SQAPafzLU68I2aQYgj590cmqbEuXhFhQV8qVfgftnR4Gd", body, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("response checked >", response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
                    @Override
                    public Map<String, String> getHeaders() {
                        Realm realm = Realm.getInstance(ctx);
                        RealmResults<UserRealm> user = realm.where(UserRealm.class).findAll();
                        Map headers = new HashMap();
                        headers.put("x-access-token", user.first().getToken());
                        realm.close();
                        return headers;

                    }
                };
                rq.add(jor);

            }
        };
    }

}
