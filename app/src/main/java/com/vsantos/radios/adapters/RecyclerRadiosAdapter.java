package com.vsantos.radios.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.vsantos.radios.BroadcastActivity;
import com.vsantos.radios.R;
import com.vsantos.radios.RadioDetailActivity;
import com.vsantos.radios.objects.RadioObject;
import com.vsantos.radios.realm.UserRealm;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by vitor on 12/19/15.
 */
public class RecyclerRadiosAdapter extends RecyclerView.Adapter<RecyclerRadiosAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    private Context ctx;
    private ArrayList<RadioObject> list;
    final private String TAG = getClass().getSimpleName();
    private Boolean global_list;

    public RecyclerRadiosAdapter(Context ctx, ArrayList<RadioObject> response, Boolean global_list) {
        this.ctx = ctx;
        this.inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = response;
        this.global_list = global_list;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = inflater.inflate(R.layout.item_list_radio, parent, false);

        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RadioObject elem = list.get(position);

        holder.name.setText(elem.getName());
        holder.name.setTextColor(Color.parseColor(elem.getText_color()));
        holder.block.setBackgroundColor(Color.parseColor(elem.getBg_color()));
        holder.city.setText(elem.getCity());

        if(list.get(position).getUser_follow()){
            holder.follow.setText("SIGUIENDO");
        }else{
            holder.follow.setText("SEGUIR");
        }

        Picasso.with(ctx).load(list.get(position).getImage_list()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RequestQueue rq;
        RelativeLayout block;
        ImageView image, btnImageBroadcast;
        TextView name, follow, city;

        public MyViewHolder(View itemView) {
            super(itemView);

            rq = Volley.newRequestQueue(ctx);

            block = (RelativeLayout) itemView.findViewById(R.id.block);
            image = (ImageView)itemView.findViewById(R.id.imageTop);
            name = (TextView) itemView.findViewById(R.id.nameRadio);
            city = (TextView) itemView.findViewById(R.id.info_extra);
            follow = (TextView) itemView.findViewById(R.id.follow);
            btnImageBroadcast = (ImageView)itemView.findViewById(R.id.btnImageBroadcast);

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx, RadioDetailActivity.class);
                    i.putExtra("_id", list.get(getAdapterPosition()).getId());
                    i.putExtra("title", list.get(getAdapterPosition()).getName());
                    i.putExtra("image_detail", list.get(getAdapterPosition()).getImage_detail());
                    i.putExtra("url_stream", list.get(getAdapterPosition()).getUrl_streaming());
                    i.putExtra("bg_color", list.get(getAdapterPosition()).getBg_color());
                    i.putExtra("text_color", list.get(getAdapterPosition()).getText_color());
                    i.putExtra("follow", list.get(getAdapterPosition()).getUser_follow());

                    ctx.startActivity(i);

                }
            });

            follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Realm realm = Realm.getInstance(ctx);
                    RealmResults<UserRealm> user = realm.where(UserRealm.class).findAll();
                    if(user.size() == 0){
                        Toast.makeText(ctx, "Esta función es solo para logueados", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    realm.close();

                    JSONObject data = null;
                    try {
                        data = new JSONObject();
                        data.put("radioId", list.get(getAdapterPosition()).getId());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String url = ctx.getString(R.string.server)+"/api/follow";

                    JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, url, data, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            String msg = null;

                            try {


                                if(response.getBoolean("follow")){
                                    msg = ctx.getString(R.string.follow_toast_msg);
                                    follow.setText(R.string.follow_block_text);
                                }else{
                                    msg = ctx.getString(R.string.unfollow_toast_msg);
                                    follow.setText(R.string.unfollow_block_text);
                                }

                                list.get(getAdapterPosition()).setUser_follow(response.getBoolean("follow"));

                                Toast.makeText(ctx, msg+list.get(getAdapterPosition()).getName(), Toast.LENGTH_SHORT).show();
                                if(!global_list){
                                    list.remove(getAdapterPosition());
                                    notifyDataSetChanged();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }){
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Realm realm = Realm.getInstance(ctx);
                            RealmResults<UserRealm> user = realm.where(UserRealm.class).findAll();
                            Map headers = new HashMap();
                            if(user.size() > 0) {
                                headers.put("x-access-token", user.first().getToken());
                            }
                            realm.close();

                            return headers;
                        }
                    };
                    rq.add(jor);

                }
            });

            btnImageBroadcast.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx, BroadcastActivity.class);
                    i.putExtra("_id", list.get(getAdapterPosition()).getId());
                    i.putExtra("name", list.get(getAdapterPosition()).getName());
                    i.putExtra("image_profile", list.get(getAdapterPosition()).getImage_profile());
                    i.putExtra("url_stream", list.get(getAdapterPosition()).getUrl_streaming());
                    Log.i(getClass().getSimpleName(), list.get(getAdapterPosition()).getUrl_streaming());
                    ctx.startActivity(i);
                }
            });

        }

        @Override
        public void onClick(View v) {

        }
    }
}
