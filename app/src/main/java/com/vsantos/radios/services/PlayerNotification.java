package com.vsantos.radios.services;

/**
 * Created by vitor on 2/20/16.
 */
import android.app.Notification;
import android.app.PendingIntent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.app.Service;
import android.content.Intent;
import android.widget.RemoteViews;

import com.vsantos.radios.MainActivity;
import com.vsantos.radios.R;
import com.vsantos.radios.utils.Utils;

import java.io.IOException;

public class PlayerNotification extends Service {
    public static MediaPlayer mediaPlayer;
    private static String url_stream;
    private Bitmap image;
    private String name;
    Notification status;
    private static RemoteViews notificationView;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction().equals(Utils.ACTION.STARTFOREGROUND_ACTION)) {

            if(url_stream == null){
                url_stream = "";
            }

            if(url_stream.equals(intent.getStringExtra("url_stream"))) {
                return START_STICKY;
            }

            //Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();
            if(mediaPlayer == null) {
                mediaPlayer = new MediaPlayer();
            }else{
                mediaPlayer.reset();
            }
            try {
                name = intent.getStringExtra("name");
                image = Utils.getBitmapFromURL(intent.getStringExtra("image_profile"));
            }catch (Exception e){
                e.printStackTrace();
            }



            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mediaPlayer.setDataSource(intent.getStringExtra("url_stream"));
                mediaPlayer.prepareAsync();
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mediaPlayer.start();
                    }
                });

                url_stream = intent.getStringExtra("url_stream");

            } catch (IOException e) {
                e.printStackTrace();
            }
            showNotification();

        } else if (intent.getAction().equals(Utils.ACTION.PLAY_ACTION)) {

            if(mediaPlayer.isPlaying()){
                mediaPlayer.pause();
                notificationView.setImageViewResource(R.id.status_bar_play, R.mipmap.apollo_holo_dark_play);
            }else{
                mediaPlayer.start();
                notificationView.setImageViewResource(R.id.status_bar_play, R.mipmap.apollo_holo_dark_pause);
            }
            status.bigContentView = notificationView;
            startForeground(Utils.NOTIFICATION_ID.FOREGROUND_SERVICE, status);

        } else if (intent.getAction().equals(Utils.ACTION.STOPFOREGROUND_ACTION)) {
            url_stream = "";
            mediaPlayer.stop();
            stopForeground(true);
            stopSelf();
        }
        return START_STICKY;
    }

    private void showNotification() {

        notificationView = new RemoteViews(getPackageName(), R.layout.player_notification);

        notificationView.setImageViewBitmap(R.id.status_bar_album_art, image);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(Utils.ACTION.MAIN_ACTION);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);


        Intent playIntent = new Intent(this, PlayerNotification.class);
        playIntent.setAction(Utils.ACTION.PLAY_ACTION);
        PendingIntent pplayIntent = PendingIntent.getService(this, 0, playIntent, 0);

        Intent closeIntent = new Intent(this, PlayerNotification.class);
        closeIntent.setAction(Utils.ACTION.STOPFOREGROUND_ACTION);
        PendingIntent pcloseIntent = PendingIntent.getService(this, 0, closeIntent, 0);

        notificationView.setOnClickPendingIntent(R.id.status_bar_play, pplayIntent);

        notificationView.setOnClickPendingIntent(R.id.status_bar_collapse, pcloseIntent);

        notificationView.setImageViewResource(R.id.status_bar_play, R.mipmap.apollo_holo_dark_pause);

        notificationView.setTextViewText(R.id.status_bar_track_name, name);

        if(status == null) {
            status = new Notification.Builder(this).build();
        }
        status.contentView = notificationView;
        status.bigContentView = notificationView;
        status.flags = Notification.FLAG_ONGOING_EVENT;
        status.icon = R.mipmap.ic_launcher;
        status.contentIntent = pendingIntent;
        startForeground(Utils.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}

