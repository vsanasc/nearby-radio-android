package com.vsantos.radios;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.squareup.picasso.Picasso;
import com.vsantos.radios.fragments.HomeFragment;
import com.vsantos.radios.fragments.NotificationFragment;
import com.vsantos.radios.fragments.programs.ProgramsFragment;
import com.vsantos.radios.fragments.radios.RadiosAllFragment;
import com.vsantos.radios.fragments.radios.RadiosFragment;
import com.vsantos.radios.gcm.RegistrationIntentService;
import com.vsantos.radios.models.User;
import com.vsantos.radios.realm.UserRealm;

import io.realm.Realm;
import io.realm.RealmResults;
import io.socket.client.Socket;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private Drawer navigationDrawer;
    private AccountHeaderBuilder headerDrawer;
    private FragmentManager fragmentManager = getSupportFragmentManager();
    //Socket mSocket;
    ProfileSettingDrawerItem profileSettings;
    ProfileDrawerItem profileDrawerItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        FacebookSdk.sdkInitialize(getApplicationContext());

        Realm realm = Realm.getInstance(this);
        RealmResults<UserRealm> userRealmResults = realm.where(UserRealm.class).findAll();

        if (userRealmResults.size() > 0) {

            profileSettings = new ProfileSettingDrawerItem().withName("Cerrar sesión").withIcon(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_add).actionBar().paddingDp(5).colorRes(R.color.material_drawer_primary_text)).withIdentifier(1);

            profileDrawerItem = new ProfileDrawerItem().withName(userRealmResults.first().getFirst_name()).withEmail(userRealmResults.first().getEmail()).withIcon(userRealmResults.first().getImage());
            DrawerImageLoader.init(new DrawerImageLoader.IDrawerImageLoader() {
                @Override
                public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                    Picasso.with(imageView.getContext()).load(uri).placeholder(placeholder).into(imageView);
                }

                @Override
                public void cancel(ImageView imageView) {
                    Picasso.with(imageView.getContext()).cancelRequest(imageView);
                }

                @Override
                public Drawable placeholder(Context ctx) {
                    return ctx.getResources().getDrawable(R.mipmap.default_profile);
                }

                @Override
                public Drawable placeholder(Context ctx, String tag) {
                    return null;
                }

            });

            //actionMenu(1, null);
        }
        realm.close();

        headerDrawer = new AccountHeaderBuilder()
                .withActivity(this)
                .withCompactStyle(true)
                .withSavedInstance(savedInstanceState)
                .withHeaderBackground(R.mipmap.bg_header_menu)
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {

                        if (profile instanceof IDrawerItem && ((IDrawerItem) profile).getIdentifier() == 1) {

                            Realm realm = Realm.getInstance(MainActivity.this);
                            RealmResults<UserRealm> realmResults = realm.where(UserRealm.class).findAll();

                            realm.beginTransaction();
                            realmResults.clear();
                            realm.commitTransaction();
                            realm.close();

                            Intent i = new Intent(MainActivity.this, LoginActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                        }

                        return false;
                    }
                });

                if(profileSettings != null){
                    headerDrawer.addProfiles(profileSettings);
                }


        navigationDrawer = new DrawerBuilder()
                                .withActivity(this)
                                .withToolbar(toolbar)
                                .withAccountHeader(headerDrawer.build())
                                .withDisplayBelowStatusBar(true)
                                .withActionBarDrawerToggleAnimated(true)
                                .withSelectedItem(1)
                                .withSavedInstance(savedInstanceState)
                                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                                    @Override
                                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                        actionMenu(drawerItem.getIdentifier(), drawerItem);
                                        return false;
                                    }
                                })
                                .build();

        navigationDrawer.addItems(
                new SecondaryDrawerItem().withName("Login").withIdentifier(0),
                new SecondaryDrawerItem().withName("Inicio").withIcon(R.mipmap.ic_home_menu).withIdentifier(1),
                new DividerDrawerItem(),
                new SecondaryDrawerItem().withName("Todas las Radios").withIcon(R.mipmap.ic_radio_menu).withIdentifier(2),
                new SecondaryDrawerItem().withName("Mis Radios").withIcon(R.mipmap.ic_radio_menu).withIdentifier(3),
                //new DividerDrawerItem(),
                //new PrimaryDrawerItem().withName("Todos los Programas").withIcon(R.mipmap.ic_programs_menu).withIdentifier(4),
                //new PrimaryDrawerItem().withName("Mis Programas").withIcon(R.mipmap.ic_programs_menu).withIdentifier(5),
                new DividerDrawerItem(),
                new SecondaryDrawerItem().withName("Notificaciones").withIcon(R.mipmap.ic_notification_menu).withIdentifier(6),
                new SecondaryDrawerItem().withName("Configuraciones").withIcon(R.mipmap.ic_notification_menu).withIdentifier(7)

        );

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        navigationDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);



        //NearbyRadioApplication app = (NearbyRadioApplication) getApplication();
        //mSocket = app.getSocket();


    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        /*
        if (mSocket != null){
            mSocket.disconnect();
        }
        */

    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }
    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void actionMenu(int  position, IDrawerItem drawerItem){

        Class fragmentClass = null;
        Fragment fragment = null;
        Bundle bundle = new Bundle();

        switch (position) {

            case 0:
                Intent i = new Intent(this, LoginActivity.class);
                startActivity(i);
                break;
            case 1:
                fragmentClass = HomeFragment.class;

                break;
            case 2:
                fragmentClass = RadiosAllFragment.class;
                toolbar.setTitle("Todas las Radios");
                bundle.putString("type", "all");
                break;
            case 3:
                fragmentClass = RadiosAllFragment.class;
                toolbar.setTitle("Mis Radios");

                bundle.putString("type", "my");
                break;

            case 4:
                fragmentClass = NotificationFragment.class;
                toolbar.setTitle("Notificaciones");
                break;

        }

        if (fragmentClass != null) {

            try {
                fragment = (Fragment)fragmentClass.newInstance();
                fragment.setArguments(bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }

            fragmentManager.popBackStackImmediate();
            fragmentManager.beginTransaction().replace(R.id.mainFrame, fragment).commit();



        }
    }

}
