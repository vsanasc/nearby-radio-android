package com.vsantos.radios.objects;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vitor on 12/22/15.
 */
public class BroadcasterObject {
    private String id;
    private String name;
    private String image;
    private String description;

    public BroadcasterObject(JSONObject data) throws JSONException {
        this.id = data.getString("_id");
        this.name = data.getString("name");
        this.image = data.getString("image");
        this.description = data.getString("description");
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getImage() {
        return image;
    }

    public String getDescription() {
        return description;
    }

}
