package com.vsantos.radios.objects;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vitor on 12/19/15.
 */
public class RadioObject {
    private String id;
    private String name;
    private String image_list;
    private String image_detail;
    private String image_profile;
    private Integer follows;
    private String city;
    private String bg_color;
    private String text_color;
    private Boolean user_follow;
    private String url_streaming;
    private String info;

    public RadioObject(JSONObject data) throws JSONException{
        this.id = data.getString("_id");
        this.name = data.getString("name");
        this.image_list = data.getString("image_list");
        this.image_detail = data.getString("image_detail");
        this.bg_color = data.getString("bg_color");
        this.text_color = data.getString("text_color");
        this.user_follow = data.getBoolean("follow");
        this.image_profile = data.getString("image_profile");
        this.url_streaming = (data.has("url_stream"))?data.getString("url_stream"):"";
        this.info = (data.has("info"))?data.getString("info"):"";
        this.city = data.getString("city");
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage_list() {
        return image_list;
    }

    public String getImage_detail() {
        return image_detail;
    }

    public Integer getFollows() {
        return follows;
    }

    public String getCity() {
        return city;
    }

    public String getBg_color() {
        return bg_color;
    }

    public String getText_color() {
        return text_color;
    }

    public Boolean getUser_follow() {
        return user_follow;
    }

    public String getUrl_streaming() {
        return url_streaming;
    }

    public String getInfo() {
        return info;
    }

    public void setUser_follow(Boolean user_follow) {
        this.user_follow = user_follow;
    }

    public String getImage_profile() {
        return image_profile;
    }
}
