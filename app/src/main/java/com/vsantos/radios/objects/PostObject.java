package com.vsantos.radios.objects;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by vitor on 1/10/16.
 */
public class PostObject {

    private String id;
    private String text;
    private String name_profile;
    private String image_profile;
    private String image;
    private String type;
    private Date date;

    public PostObject(JSONObject data) throws JSONException{
        this.id = data.getString("_id");
        this.text = data.getString("text");

        this.type = data.getString("type");
        switch (this.type){
            case "youtube":
                JSONObject extra = data.getJSONObject("extra");
                this.image = "http://img.youtube.com/vi/"+ extra.getJSONObject("snippet").getJSONObject("resourceId").getString("videoId") +"/hqdefault.jpg";
                break;

            case "post":
                this.image = data.getString("image");

                break;
        }

        this.image_profile = data.getJSONObject("radio").getString("image_profile");
        this.name_profile = data.getJSONObject("radio").getString("name");
        this.date = new Date(data.getInt("created_at_timestamp"));

    }

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getImage() {
        return image;
    }

    public String getName_profile() {
        return name_profile;
    }

    public String getImage_profile() {
        return image_profile;
    }

    public String getType() {
        return type;
    }

    public Date getDate() {
        return date;
    }
}
