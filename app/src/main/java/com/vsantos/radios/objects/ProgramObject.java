package com.vsantos.radios.objects;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vitor on 12/19/15.
 */
public class ProgramObject {
    private String id;
    private String name;
    private String image_list;
    private String image_detail;
    private Integer follows;
    private String bg_color;
    private String topic;
    private String text_color;
    private String radio;
    private Boolean user_follow;
    private String info;

    public ProgramObject(JSONObject data) throws JSONException{
        this.id = data.getString("_id");
        this.name = data.getString("name");
        this.image_list = data.getString("image_list");
        this.image_detail = data.getString("image_detail");
        this.bg_color = data.getString("bg_color");
        this.text_color = data.getString("text_color");
        this.radio = data.getString("radio");
        this.user_follow = data.getBoolean("follow");
        this.info = (data.has("info"))?data.getString("info"):"";
        this.topic = data.getString("topic");
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage_list() {
        return image_list;
    }

    public String getImage_detail() {
        return image_detail;
    }

    public Integer getFollows() {
        return follows;
    }

    public String getTopic() {
        return topic;
    }

    public String getBg_color() {
        return bg_color;
    }

    public String getText_color() {
        return text_color;
    }

    public String getRadio() {
        return radio;
    }

    public Boolean getUser_follow() {
        return user_follow;
    }

    public String getInfo() {
        return info;
    }

    public void setUser_follow(Boolean user_follow) {
        this.user_follow = user_follow;
    }
}
