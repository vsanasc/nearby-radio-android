package com.vsantos.radios.objects;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vitor on 2/5/16.
 */
public class TimelineObject {

    private String id;
    private String type;
    private String imageProfile;
    private String nameProfile;
    private String imageContent;
    private String text;

    public TimelineObject(JSONObject data) throws JSONException{

        this.id = data.getString("_id");
        this.type = data.getString("type");

        switch (this.type){

            case "start":
                this.setStartElement(data);
                break;

            case "text":
                this.setTextElement(data);
                break;
            case "youtube":
                this.setYotubeElement(data);
                break;
            case "image":
                this.setTextElement(data);
                break;
        }

    }
    private void setTextElement(JSONObject data) throws JSONException{
        this.imageProfile = data.getJSONObject("user").getString("image");
        this.nameProfile = data.getJSONObject("user").getString("first_name")+" "+data.getJSONObject("user").getString("last_name");
        this.imageContent = data.getString("image");
        this.text = data.getString("text");
    }
    private void setYotubeElement(JSONObject data) throws JSONException{
        this.imageProfile = data.getJSONObject("user").getString("image");
        this.nameProfile = data.getJSONObject("user").getString("first_name")+" "+data.getJSONObject("user").getString("last_name");
        this.imageContent = "http://img.youtube.com/vi/"+ data.getString("text") +"/hqdefault.jpg";
        this.text = "";
    }
    private void setStartElement(JSONObject data) throws JSONException{
        JSONObject program = data.getJSONObject("broadcast").getJSONObject("program");

        this.imageProfile = program.getString("image_profile");
        this.text = program.getString("name")+ " - " +data.getJSONObject("broadcast").getString("title");
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getImageProfile() {
        return imageProfile;
    }

    public String getNameProfile() {
        return nameProfile;
    }

    public String getImageContent() {
        return imageContent;
    }

    public String getText() {
        return text;
    }
}
