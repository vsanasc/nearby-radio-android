package com.vsantos.radios.objects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vitor on 3/5/16.
 */
public class NotificationObject {
    String name;
    String image;
    JSONArray configs;

    public NotificationObject(JSONObject data) throws JSONException {
        JSONObject elem = null;
        if(data.has("radio")){
            elem = data.getJSONObject("radio");
        }else{
            elem = data.getJSONObject("program");
        }

        this.name = elem.getString("name");
        this.image = elem.getString("image_profile");
        this.configs = data.getJSONArray("configs");
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public JSONArray getConfigs() {
        return configs;
    }
}
